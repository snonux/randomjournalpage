# Read a random journal

This is a quick and dirty script which I use personally to grab a random PDF file (a scanned version of one of my bullet journals) and to extract a random set of pages from it in order to reflect/read what was happening in the past. This also includes various notes of books I have read and random ideas I wrote down and my want to reconsider.

## Requirements

For Fedora Linux:

```
# for (obviously) qpdf
sudo dnf install qpdf
# for pdfinfo
sudo dnf install poppler-utils
```

## Usage

Simply configure the journal path and the PDF vewer in the script and run: `./randomjournalpage.sh`

## Testing

There is no real testing done. It's a pretty simple script. However, the script passes the following ShellCheck command:

```
shellcheck randomjournalpage.sh --exclude SC2155
```
